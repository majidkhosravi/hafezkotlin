package model

data class TabModel(val code: String, val title: String)