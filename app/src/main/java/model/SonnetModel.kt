package model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Sonnet")
data class SonnetModel(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    var id: Int?,


    @ColumnInfo(name = "content")
    var content: String?,


    @ColumnInfo(name = "favorite")
    var favorite: Int?,

    @ColumnInfo(name = "seen")
    var seen: Int?,


    @ColumnInfo(name = "interpretation")
    var interpretation: String?,


//    @Ignore
    @ColumnInfo(name = "firstCouplet")
    var firstCouplet: String?,


//    @Ignore
    @ColumnInfo(name = "reserve")
    var reserve: String?,

//    @Ignore
    @ColumnInfo(name = "firstHemistich")
    var firstHemistich: String?

) : Parcelable {
    constructor(id: Int) : this(id,
        null,
        null,
        null,
        null,
        null,
        null,
        null)
}