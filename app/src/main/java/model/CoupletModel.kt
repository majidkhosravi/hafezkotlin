package model

data class CoupletModel(val couplet: String) {

    fun getCouplet(): SimpleCouplet {
        val array = couplet.split("\n")
        return SimpleCouplet(array[0], array[1])
    }

    class SimpleCouplet(
        val firstHemistich: String,
        val secondHemistich: String
    )

}