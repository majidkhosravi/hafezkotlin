package ui.base

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import ir.hafez.poem.withvoice.BR
import viewmodel.BaseViewModel
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<T : BaseViewModel, R : ViewDataBinding> : AppCompatActivity() {

    protected lateinit var viewModel: T
    protected var binding: R? = null
    protected var viewModelClassType =
        (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showLog(message = "onCreate")
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        if (intent != null) gatherData(intent)

        viewModel = createViewModel()
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        binding?.lifecycleOwner = this
        binding?.setVariable(BR.viewModel, viewModel)

        doOtherTask()
    }


    override fun onStart() {
        super.onStart()
        showLog(message = "onStart")
    }

    override fun onResume() {
        super.onResume()
        showLog(message = "onResume")
    }


    override fun onPause() {
        super.onPause()
        showLog(message = "onPause")
    }

    override fun onStop() {
        super.onStop()
        showLog(message = "onStop")
    }


    override fun onDestroy() {
        super.onDestroy()
        showLog(message = "onDestroy")
    }


    private fun showLog(tag: String = "Majid", message: String) {
        Log.i(tag, "${javaClass.simpleName} : $message")
    }


    open fun gatherData(intent: Intent) {}

    abstract fun doOtherTask()

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun createViewModel(fragmentActivity: FragmentActivity = this): T

    open fun createSimpleViewModel(fragmentActivity: FragmentActivity = this) =
        ViewModelProviders.of(fragmentActivity).get(viewModelClassType)

}