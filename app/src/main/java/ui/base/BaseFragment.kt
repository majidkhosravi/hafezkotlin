package ui.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import ir.hafez.poem.withvoice.BR
import viewmodel.BaseViewModel
import java.lang.reflect.ParameterizedType


abstract class BaseFragment<T : BaseViewModel, R : ViewDataBinding> : Fragment() {

    protected lateinit var binding: R
    protected lateinit var viewModel: T
    protected var viewModelClassType =
        (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        showLog(message = "onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showLog(message = "onCreate")

        activity?.let {
            viewModel = createViewModel(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        showLog(message = "onCreateView")
        binding = DataBindingUtil.inflate(
            inflater,
            getLayoutResource(),
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.setVariable(BR.viewModel, this.viewModel)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showLog(message = "onViewCreated")

        if (savedInstanceState != null) gatherData(savedInstanceState)

        doOtherTask()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showLog(message = "onActivityCreated")
    }

    override fun onStart() {
        super.onStart()
        showLog(message = "onStart")
    }

    override fun onResume() {
        super.onResume()
        showLog(message = "onResume")
    }

    override fun onPause() {
        super.onPause()
        showLog(message = "onPause")
    }

    override fun onStop() {
        super.onStop()
        showLog(message = "onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        showLog(message = "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        showLog(message = "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        showLog(message = "onDetach")
    }

    protected fun isViewModelInitialized() = ::viewModel.isInitialized

    private fun showLog(tag: String = "Majid", message: String) {
        Log.i(tag, "${javaClass.simpleName} : $message")
    }

    @LayoutRes
    abstract fun getLayoutResource(): Int

    abstract fun createViewModel(it: FragmentActivity): T

    open fun createSimpleViewModel(it: FragmentActivity = activity!!): T =
        ViewModelProviders.of(it).get(viewModelClassType)

    abstract fun gatherData(bundle: Bundle)

    abstract fun doOtherTask()

}