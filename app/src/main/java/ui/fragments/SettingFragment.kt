package ui.fragments

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import ir.hafez.poem.withvoice.R
import ir.hafez.poem.withvoice.databinding.FragmentSettingBinding
import ui.base.BaseFragment
import viewmodel.SettingViewModel

class SettingFragment : BaseFragment<SettingViewModel, FragmentSettingBinding>() {

    override fun getLayoutResource(): Int = R.layout.fragment_setting

    override fun createViewModel(it: FragmentActivity) = createSimpleViewModel()

    override fun gatherData(bundle: Bundle) {}

    override fun doOtherTask() {}

}