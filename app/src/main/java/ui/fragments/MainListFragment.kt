package ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import ir.hafez.poem.withvoice.R
import ir.hafez.poem.withvoice.databinding.FragmentMainListBinding
import kotlinx.android.synthetic.main.fragment_main_list.*
import model.SonnetModel
import ui.base.BaseFragment
import utils.ActivityUtil
import viewmodel.SonnetViewModel
import widgets.RtlSearchView

class MainListFragment : BaseFragment<SonnetViewModel, FragmentMainListBinding>() {

    private var lastItemSelectedId: Int = 0
    private var lastSearchQuery: String = ""
    private val mConsumerObserver = Consumer(this::showSonnetList)
    private val mSelectedItemModelObserver =
        Observer<SonnetModel> {
            if (it != null) ActivityUtil.showSonnetDetailActivity(activity!!, it, lastSearchQuery)
        }

    override fun getLayoutResource(): Int = R.layout.fragment_main_list

    override fun createViewModel(it: FragmentActivity) = createSimpleViewModel()

    override fun gatherData(bundle: Bundle) {}

    override fun doOtherTask() {
        rv_main.layoutManager = LinearLayoutManager(context)

        viewModel.mSelectedItem.observe(this, mSelectedItemModelObserver)

        search_bar.setTitleText(getString(R.string.app_name))
        search_bar.setOnTextChangedListener(object : RtlSearchView.OnTextChangedListener {
            override fun onTextChanged(query: String) {
                getSonnetList(lastItemSelectedId, query)
            }
        })
    }

    override fun onResume() {
        super.onResume()
//        search_bar?.clearSearchBar()
    }

    private fun showSonnetList(it: List<SonnetModel>) {
        progress_loading?.visibility = View.GONE
        if (it.isNullOrEmpty()) {
            text_error.text = getString(R.string.no_item_found)
            text_error.visibility = View.VISIBLE
        } else {
            text_error.text = ""
            rv_main.visibility = View.VISIBLE
            viewModel.createListAdapter(it)
        }
    }


    fun getSonnetList(itemId: Int, searchQuery: String = lastSearchQuery) {
        rv_main?.visibility = View.GONE
        text_error?.visibility = View.GONE
        progress_loading?.visibility = View.VISIBLE

        lastItemSelectedId = itemId
        lastSearchQuery = searchQuery
        if (searchQuery.isEmpty()) search_bar?.clearSearchBar()
//        if (::viewModel.isInitialized.not()) return
        when (itemId) {
            R.id.navigation_home ->
                viewModel.getAllSonnets(lastSearchQuery)?.subscribeOn(Schedulers.io())
                    ?.observeOn(AndroidSchedulers.mainThread())?.subscribe(mConsumerObserver)

            R.id.navigation_seen ->
                viewModel.getSeenSonnet(lastSearchQuery)?.subscribeOn(Schedulers.io())
                    ?.observeOn(AndroidSchedulers.mainThread())?.subscribe(mConsumerObserver)

            R.id.navigation_favorite ->
                viewModel.getLikedSonnets(lastSearchQuery)?.subscribeOn(Schedulers.io())
                    ?.observeOn(AndroidSchedulers.mainThread())?.subscribe(mConsumerObserver)
        }
    }

}

