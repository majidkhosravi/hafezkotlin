package ui.fragments

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import ir.hafez.poem.withvoice.R
import ir.hafez.poem.withvoice.databinding.FragmentRandomSonnetBinding
import ui.base.BaseFragment
import utils.ActivityUtil
import viewmodel.SonnetViewModel

class RandomSonnetFragment : BaseFragment<SonnetViewModel, FragmentRandomSonnetBinding>() {

    override fun getLayoutResource(): Int = R.layout.fragment_random_sonnet

    override fun createViewModel(it: FragmentActivity) = createSimpleViewModel()

    override fun gatherData(bundle: Bundle) {}

    override fun doOtherTask() {
        viewModel.mSelectedItem.observe(this, Observer {
            if (it != null) ActivityUtil.showSonnetDetailActivity(activity!!, it)
        })
    }

}