package ui.activities

import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import ir.hafez.poem.withvoice.R
import ir.hafez.poem.withvoice.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import model.TabModel
import ui.base.BaseActivity
import ui.base.BaseFragment
import ui.fragments.MainListFragment
import ui.fragments.RandomSonnetFragment
import ui.fragments.SettingFragment
import utils.ActivityUtil
import viewmodel.SonnetViewModel


class MainActivity : BaseActivity<SonnetViewModel, ActivityMainBinding>(),
    BottomNavigationView.OnNavigationItemSelectedListener {
    private var lastItemSelectedId: Int = 0
    private var mMainListFragment: MainListFragment? = null
    private var mSettingFragment: SettingFragment? = null
    private var mRandomSonnetFragment: RandomSonnetFragment? = null

    override fun doOtherTask() {
        if (mMainListFragment == null) mMainListFragment = MainListFragment()
        replaceFragment(mMainListFragment!!)

        setupBottomNavigation()
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun createViewModel(fragmentActivity: FragmentActivity) = createSimpleViewModel()

    override fun onStart() {
        super.onStart()
        bottom_navigation.selectedItemId = lastItemSelectedId
    }

    private fun setupBottomNavigation() {
        var activeTabs: Array<TabModel?>? = null
        if (activeTabs == null) {
            val tabCodes = resources.getStringArray(R.array.codes)
            val tabTitles = resources.getStringArray(R.array.tabs)
            val itemCounts = tabCodes.size
            activeTabs = arrayOfNulls(tabCodes.size)
            for (i in 0 until itemCounts) {
                activeTabs[i] = TabModel(
                    tabCodes[i],
                    tabTitles[i]
                )
            }
        }

        var lastId = 0
        val tabsCount = activeTabs.size
        for (i in tabsCount - 1 downTo 0) {
            val currentTab: TabModel? = activeTabs[i]
            addNewTab(
                getProperId(currentTab?.code!!),
                currentTab.title,
                getProperIcon(currentTab.code)
            )
            if (i == 0) lastId = getProperId(currentTab.code)
        }
        bottom_navigation.setOnNavigationItemSelectedListener(this)
        lastItemSelectedId = lastId
    }

    private fun addNewTab(
        @IdRes id: Int, title: CharSequence,
        @DrawableRes icon: Int
    ) {
        bottom_navigation.menu.add(Menu.NONE, id, Menu.NONE, title).setIcon(icon)
    }

    @IdRes
    private fun getProperId(code: String): Int {
        return if (TextUtils.isEmpty(code)) R.id.no_id
        else when (code) {
            getString(R.string.home_code) -> R.id.navigation_home
            getString(R.string.seen_code) -> R.id.navigation_seen
            getString(R.string.random_code) -> R.id.navigation_random
            getString(R.string.favorite_code) -> R.id.navigation_favorite
            getString(R.string.setting_code) -> R.id.navigation_setting
            else -> R.id.no_id
        }
    }

    @DrawableRes
    private fun getProperIcon(code: String): Int {
        return if (TextUtils.isEmpty(code)) R.drawable.ic_home
        else when (code) {
            getString(R.string.home_code) -> R.drawable.ic_home
            getString(R.string.seen_code) -> R.drawable.ic_seen
            getString(R.string.random_code) -> R.drawable.round_menu_book_white_48
            getString(R.string.favorite_code) -> R.drawable.ic_favorite
            getString(R.string.setting_code) -> R.drawable.ic_setting
            else -> R.drawable.ic_home
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        lastItemSelectedId = item.itemId
        when (item.itemId) {
            R.id.navigation_setting -> {
                if (mSettingFragment == null) mSettingFragment = SettingFragment()
                replaceFragment(mSettingFragment!!)
            }
            R.id.navigation_random -> {
                if (mRandomSonnetFragment == null) mRandomSonnetFragment = RandomSonnetFragment()
                replaceFragment(mRandomSonnetFragment!!)
            }
            else -> {
                replaceFragment(mMainListFragment!!)
                mMainListFragment?.getSonnetList(item.itemId)
            }
        }
        return true
    }

    private fun replaceFragment(fragment: BaseFragment<*, *>) {
        ActivityUtil.replaceFragment(
            supportFragmentManager,
            R.id.frame_container,
            fragment,
            fragment::class.java.simpleName
        )
    }

}
