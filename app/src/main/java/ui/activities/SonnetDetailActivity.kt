package ui.activities

import android.content.Intent
import android.text.method.ScrollingMovementMethod
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import ir.hafez.poem.withvoice.R
import ir.hafez.poem.withvoice.databinding.ActivitySonnetDetailBinding
import kotlinx.android.synthetic.main.activity_sonnet_detail.*
import model.SonnetModel
import ui.base.BaseActivity
import utils.ActivityUtil
import viewmodel.SonnetDetailViewModel


class SonnetDetailActivity : BaseActivity<SonnetDetailViewModel, ActivitySonnetDetailBinding>() {

    private var mSonnetModel: SonnetModel? = null
    private var mSonnetId: Int? = null
    private var searchKeyword: String? = null

    private val mBackEventObserver = Observer<Boolean> {
        onBackPressed()
    }
    private val mFindSonnetObserver = Observer<SonnetModel> {
        viewModel.setSonnetModel(it, searchKeyword!!)
    }

    override fun gatherData(intent: Intent) {
        mSonnetModel = intent.getParcelableExtra(ActivityUtil.Argument.prc_sonnet)
        mSonnetId = intent.getIntExtra(ActivityUtil.Argument.srl_int, -1)
        searchKeyword = intent.getStringExtra(ActivityUtil.Argument.srl_str)
    }

    override fun doOtherTask() {
        rv_content.layoutManager = LinearLayoutManager(this)
        text_interpretation.movementMethod = ScrollingMovementMethod()

        viewModel.mBackSingleEvent.observe(this, mBackEventObserver)

        if (mSonnetId != null && mSonnetId != -1) {
            viewModel.findSonnetById(mSonnetId!!)?.observe(this, mFindSonnetObserver)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_sonnet_detail

    override fun createViewModel(fragmentActivity: FragmentActivity) = createSimpleViewModel()

}