package db

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.SonnetModel
import kotlin.coroutines.CoroutineContext

class SonnetsRepository(application: Application) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var sonnetsDao: SonnetDao? = null

    init {
        val db = SonnetDataBase.getInstance(application)
        sonnetsDao = db.sonnetDao()
    }

    fun findSonnetsById(sonnetId: Int) = sonnetsDao?.findSonnetById(sonnetId)

    fun getAllSonnets(searchQuery: String) = sonnetsDao?.getAllSonnets(searchQuery)

    fun getLikedSonnets(searchQuery: String) = sonnetsDao?.getLikedSonnets(searchQuery)

    fun getSeenSonnets(searchQuery: String) = sonnetsDao?.getSeenSonnets(searchQuery)

//    fun searchSonnets(query: String) = sonnetDao?.searchSonnets(query)

    fun updateSonnet(sonnet: SonnetModel) {
        launch {
            updateSonnetBG(sonnet)
        }
    }

    fun updateFavoriteState(sonnetId: Int, fave: Int) {
        launch {
            updateFavoriteStateBG(sonnetId, fave)
        }
    }

    fun updateSeenState(sonnetId: Int, seen: Int) {
        launch {
            updateSeenStateBG(sonnetId, seen)
        }
    }


    private suspend fun updateSonnetBG(sonnet: SonnetModel) {
        withContext(Dispatchers.IO) {
            sonnetsDao?.UpdateSonnet(sonnet)
        }
    }

    private suspend fun updateFavoriteStateBG(sonnetId: Int, fave: Int) {
        withContext(Dispatchers.IO) {
            sonnetsDao?.updateFavoriteState(sonnetId, fave)
        }
    }

    private suspend fun updateSeenStateBG(sonnetId: Int, seen: Int) {
        withContext(Dispatchers.IO) {
            sonnetsDao?.updateSeenState(sonnetId, seen)
        }
    }






    fun setMessage(message: SonnetModel) {
        launch {
            setMessageBG(message)
        }
    }

    private suspend fun setMessageBG(message: SonnetModel) {
        withContext(Dispatchers.IO) {
            sonnetsDao?.setMessage(message)
        }
    }

}