package db

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.DatabaseConfiguration
import androidx.room.Room
import androidx.room.RoomDatabase
import model.SonnetModel
import java.io.*


@Database(entities = [SonnetModel::class], version = 1, exportSchema = false)
abstract class SonnetDataBase : RoomDatabase() {

    abstract fun sonnetDao(): SonnetDao

    companion object {
        const val dbName = "HafezDB"

        private var INSTANCE: SonnetDataBase? = null

        fun getInstance(context: Context): SonnetDataBase {
            synchronized(this) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        SonnetDataBase::class.java,
                        dbName
                    ).fallbackToDestructiveMigration()
                        .build()
                }
                return INSTANCE!!
            }
        }
    }


    override fun init(configuration: DatabaseConfiguration) {
        importExistingDatabase(configuration.context, true)
        super.init(configuration)
    }

    private fun importExistingDatabase(context: Context, throw_exception: Boolean) {
        val bufferSize = 32768
        val dbPath: File = context.getDatabasePath(dbName)
        if (dbPath.exists()) {
            return  // Database already exists
        }
        // Just in case make the directories
        val dirs = File(dbPath.parent!!)
        dirs.mkdirs()
        var stage = 0
        val buffer = ByteArray(bufferSize)
        var totalBytesRead: Long = 0
        var totalBytesWritten: Long = 0
        var bytesRead: Int
        try {
            val assetDB: InputStream = context.assets.open(dbName)
            stage++
            dbPath.createNewFile()
            stage++
            val realDB: OutputStream = FileOutputStream(dbPath)
            stage++
            if (assetDB == null) return
            while (assetDB.read(buffer).also { bytesRead = it } > 0) {
                totalBytesRead += bytesRead
                realDB.write(buffer, 0, bytesRead)
                totalBytesWritten += bytesRead
            }
            stage++
            realDB.flush()
            stage++
            assetDB.close()
            stage++
            realDB.close()
            stage++
        } catch (e: IOException) {
            var failedAt = ""
            when (stage) {
                0 -> failedAt = "Opening Asset ${dbName}"
                1 -> failedAt = "Creating Output Database " + dbPath.getAbsolutePath()
                2 -> failedAt = "Genreating Database OutputStream " + dbPath.getAbsolutePath()
                3 -> failedAt = "Copying Data from Asset Database to Output Database. " +
                        " Bytes read=" + totalBytesRead.toString() +
                        " Bytes written=" + totalBytesWritten.toString()
                4 -> failedAt = "Flushing Written Data (" + totalBytesWritten.toString() +
                        " bytes written)"
                5 -> failedAt = "Closing Asset Database File."
                6 -> failedAt = "Closing Created Database File."
            }
            val msg = "An error was encountered copying the Database " +
                    "from the asset file to New Database. " +
                    "The error was encountered whilst :-\n\t" + failedAt
            Log.e("IMPORTDATABASE", msg)
            e.printStackTrace()
            if (throw_exception) {
                throw RuntimeException(msg)
            }
        }
    }
}