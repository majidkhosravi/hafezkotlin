package db

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Single
import model.SonnetModel

@Dao
interface SonnetDao {

    /*  Code
      FirstCouplet
      Content
      Favorite
      Seen
      FirstHemistich
      Meaning

      */

    companion object {
        const val tableName = "Sonnet"
        //        const val neededValue = "id, firstCouplet, content, favorite, seen, firstHemistich, interpretation"
        const val neededValueToShow = "id, content, favorite, seen, interpretation"
        const val neededValueToMainList = "id, firstHemistich, favorite, seen"
        //const val neededValueToMainList = "id, FirstBeyt, favorite, seen"
    }


    // region --- Data Fetch Functions
    // Find one Row by id.

    @Query("SELECT $neededValueToShow FROM $tableName WHERE id LIKE :sonnetId")
    fun findSonnetById(sonnetId: Int): LiveData<SonnetModel>


    // Fetch All Rows unconditionally.
    @Query("SELECT $neededValueToMainList FROM $tableName  WHERE  content LIKE '%' || :searchQuery || '%'")
    fun getAllSonnets(searchQuery: String): Single<List<SonnetModel>>


    // Fetch Has Favorite Rows.
    @Query("SELECT $neededValueToMainList FROM $tableName WHERE favorite LIKE 1 AND  content LIKE '%' || :searchQuery || '%'")
    fun getLikedSonnets(searchQuery: String): Single<List<SonnetModel>>


    // Fetch Has Seen Rows.
    @Query("SELECT $neededValueToMainList FROM $tableName where seen LIKE 1 AND  content LIKE '%' || :searchQuery || '%'")
    fun getSeenSonnets(searchQuery: String): Single<List<SonnetModel>>


   /* // Fetch Has Seen Rows.
    @Query("SELECT $neededValueToMainList FROM $tableName where Content LIKE '%' || :searchQuery || '%'")
    fun searchSonnets(searchQuery: String): LiveData<List<SonnetModel>>*/

    //endregion


    // region ---  Data Update Functions
    // Update Row
    @Update
    fun UpdateSonnet(sonnet: SonnetModel)


    // Update Sonnet Favorite State
    @Query("UPDATE $tableName SET Favorite = :fave WHERE id LIKE :sonnetId")
    fun updateFavoriteState(sonnetId: Int, fave: Int)


    // Update Sonnet Favorite State
    @Query("UPDATE $tableName SET Seen = :seen WHERE id LIKE :sonnetId")
    fun updateSeenState(sonnetId: Int, seen: Int)
    // endregion


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setMessage(sonnet: SonnetModel)

/*    @RawQuery
    fun getSonnetViaQuery(query: SupportSQLiteQuery): LiveData<SonnetModel>*/

}