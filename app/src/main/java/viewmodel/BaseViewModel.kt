package viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import db.SonnetsRepository

abstract class BaseViewModel(app: Application) : AndroidViewModel(app) {
    protected val repository = SonnetsRepository(app)

    protected fun showLog(message: String) {
        Log.i(BaseViewModel::class.java.simpleName, message)
    }

}