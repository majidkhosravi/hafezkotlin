package viewmodel

import adapters.MainListAdapter
import adapters.CoupletsAdapter
import android.app.Application
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.hafez.poem.withvoice.R
import model.CoupletModel
import model.SonnetModel

class SonnetDetailViewModel(app: Application) : BaseViewModel(app) {

    var mSonnetModel: MutableLiveData<SonnetModel> = MutableLiveData()
    var mCoupletAdapter: MutableLiveData<CoupletsAdapter> = MutableLiveData()
    val mBackSingleEvent: SingleLiveEvent<Boolean> = SingleLiveEvent()
    private var mIsFirstObserve = true
    private val mClickListener = object : MainListAdapter.OnItemClickListener {
        override fun onItemClick(model: Any) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.btn_back -> mBackSingleEvent.call()

            R.id.btn_favorite -> {
                updateFavoriteState(
                    mSonnetModel.value?.id!!, mSonnetModel.value?.favorite != 1
                )
            }
        }
    }

    fun findSonnetById(sonnetCode: Int): LiveData<SonnetModel>? {
        return repository.findSonnetsById(sonnetCode)
    }

    fun setSonnetModel(it: SonnetModel, searchKeyword: String = "") {
        showLog("new Observe ,  favorite: " + it.favorite.toString())
        if (mIsFirstObserve) {
            createCoupletAdapter(it, searchKeyword)
            updateSeenState(it.id!!, true)
            mIsFirstObserve = false
        }
        mSonnetModel.value = it
    }


    private fun createCoupletAdapter(sonnetModel: SonnetModel, searchKeyword: String) {
        val list = getCoupletsList(sonnetModel)
        mCoupletAdapter.value = CoupletsAdapter(list,searchKeyword, mClickListener)
    }

    private fun updateFavoriteState(sonnetId: Int, fave: Boolean) =
        repository.updateFavoriteState(sonnetId, if (fave) 1 else 0)

    private fun updateSeenState(sonnetId: Int, seen: Boolean) =
        repository.updateSeenState(sonnetId, if (seen) 1 else 0)

    private fun getCoupletsList(sonnetModel: SonnetModel): List<CoupletModel> {
        val list = mutableListOf<CoupletModel>()
        val array = sonnetModel.content?.split("\n\n")
        for (s in array!!) list.add(CoupletModel(s))
        return list
    }


}