package viewmodel

import adapters.MainListAdapter
import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import model.SonnetModel
import kotlin.random.Random

class SonnetViewModel(app: Application) : BaseViewModel(app) {

    var mMainAdapterLiveData: MutableLiveData<MainListAdapter> = MutableLiveData()
    var mSelectedItem: SingleLiveEvent<SonnetModel> = SingleLiveEvent()
    private val mItemClickListener = object : MainListAdapter.OnItemClickListener {
        override fun onItemClick(model: Any) = setSelectedSonnet(model as SonnetModel)
    }

    fun createListAdapter(list: List<SonnetModel>) {
        if (mMainAdapterLiveData.value == null) {
            mMainAdapterLiveData.value = MainListAdapter(list, mItemClickListener)
        } else {
            mMainAdapterLiveData.value?.list = list
            mMainAdapterLiveData.value?.notifyDataSetChanged()
        }
    }

    fun getAllSonnets(searchQuery: String = "") = repository.getAllSonnets(searchQuery)

    fun getLikedSonnets(searchQuery: String = "") = repository.getLikedSonnets(searchQuery)

    fun getSeenSonnet(searchQuery: String = "") = repository.getSeenSonnets(searchQuery)

//    fun searchSonnets(query: String) = repository.searchSonnets(query)

    fun updateSonnets(sonnet: SonnetModel) = repository.updateSonnet(sonnet)

    fun setMessage(message: SonnetModel) {
        repository.setMessage(message)
    }

    val onRandomClickListener = View.OnLongClickListener {
        val firstSonnetId = 1
        val lastSonnetId = 495
        val randomId: Int = Random.nextInt(firstSonnetId, lastSonnetId)
        showLog("random sonnet id is: $randomId")
        if (randomId < firstSonnetId || randomId > lastSonnetId) return@OnLongClickListener true
        val model = SonnetModel(randomId)

        setSelectedSonnet(model)
        true
    }

    private fun setSelectedSonnet(model: SonnetModel) {
        mSelectedItem.postValue(model)
        mSelectedItem.call()
    }
}