package adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import ir.hafez.poem.withvoice.R
import model.SonnetModel

class MainListAdapter(
    var list: List<SonnetModel>,
    private val listener: OnItemClickListener
) :
    RecyclerView.Adapter<MainListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.view_list_item_row,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = if (!list.isNullOrEmpty()) list.size else 0

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (!list.isNullOrEmpty()) {
            holder.bind(list[position])
            holder.itemView.setOnClickListener {
                listener.onItemClick(list[position])
            }
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var mTitle: AppCompatTextView? = null
        private var mNumber: AppCompatTextView? = null
        private var mFavoriteIcon: AppCompatImageView? = null
        private var mSeenIcon: AppCompatImageView? = null

        init {
            mTitle = itemView.findViewById(R.id.text_title)
            mNumber = itemView.findViewById(R.id.text_number)
            mFavoriteIcon = itemView.findViewById(R.id.icon_favorite)
            mSeenIcon = itemView.findViewById(R.id.icon_seen)
        }

        fun bind(model: SonnetModel) {
            mTitle?.text = model.firstHemistich
            mNumber?.text = model.id.toString()
            mFavoriteIcon?.visibility = if (model.favorite!! == 0) View.GONE else View.VISIBLE
            mSeenIcon?.visibility = if (model.seen!! == 0) View.GONE else View.VISIBLE
        }
    }

    interface OnItemClickListener {
        fun onItemClick(model: Any)
    }
}

