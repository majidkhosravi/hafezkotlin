package adapters

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import ir.hafez.poem.withvoice.R
import model.CoupletModel

class CoupletsAdapter(
    private val list: List<CoupletModel>,
    private val searchKeyword: String,
    val listener: MainListAdapter.OnItemClickListener
) :
    RecyclerView.Adapter<CoupletsAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.view_item_content,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = if (list.isNullOrEmpty()) 0 else list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(list[position], searchKeyword)
        holder.itemView.setOnClickListener { listener.onItemClick(list[position]) }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var firstHemistich: AppCompatTextView? = null
        private var secondHemistich: AppCompatTextView? = null
        private var resource = itemView.resources

        init {
            firstHemistich = itemView.findViewById(R.id.firstHemistich)
            secondHemistich = itemView.findViewById(R.id.secondHemistich)
        }

        fun bind(coupletModel: CoupletModel, searchKeyword: String) {
            val model = coupletModel.getCouplet()
            firstHemistich?.text = model.firstHemistich
            secondHemistich?.text = model.secondHemistich

            if (TextUtils.isEmpty(searchKeyword)) return
            if (model.firstHemistich.contains(searchKeyword))
                firstHemistich?.text = getSpannable(model.firstHemistich, searchKeyword)

            if (model.secondHemistich.contains(searchKeyword))
                secondHemistich?.text = getSpannable(model.secondHemistich, searchKeyword)
        }

        private fun getSpannable(text: String, keyword: String): SpannableString {
            val spannableString = SpannableString(text)
            val start: Int = text.indexOf(keyword)
            val end: Int = start + keyword.length
            spannableString.setSpan(
                StyleSpan(Typeface.BOLD),
                start,
                end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spannableString.setSpan(
                ForegroundColorSpan(resource.getColor(R.color.colorPrimary)),
                start,
                end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return spannableString
        }
    }


}