package utils

import adapters.MainListAdapter
import adapters.CoupletsAdapter
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.hafez.poem.withvoice.R
import model.SonnetModel

class BindingAdapterUtils {


    companion object {

        @JvmStatic
        @BindingAdapter("coupletAdapter")
        fun RecyclerView.setCoupletAdapter(coupletAdapter: CoupletsAdapter?) {
            if (coupletAdapter != null)
                this.adapter = coupletAdapter
        }

        @JvmStatic
        @BindingAdapter("mainListAdapter")
        fun RecyclerView.setMainListAdapter(mainListAdapter: MainListAdapter?) {
            if (mainListAdapter != null)
                this.adapter = mainListAdapter
            this.adapter?.notifyDataSetChanged()
        }


        @JvmStatic
        @BindingAdapter("updateFavorite")
        fun AppCompatImageView.setUpdateFavorite(sonnetModel: SonnetModel?) {
            if (sonnetModel != null)
                this.setImageResource(
                    if (sonnetModel.favorite == 1) R.drawable.round_favorite_white_48
                    else R.drawable.round_favorite_border_white_48
                )
        }


        @JvmStatic
        @BindingAdapter("onRandomViewClick")
        fun View.onRandomViewClick(listener: View.OnLongClickListener) {
            this.setOnLongClickListener(listener)
        }
    }

}