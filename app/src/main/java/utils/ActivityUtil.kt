package utils

import android.app.Activity
import android.content.Intent
import androidx.annotation.IdRes
import androidx.fragment.app.FragmentManager
import model.SonnetModel
import ui.activities.SonnetDetailActivity
import ui.base.BaseFragment

class ActivityUtil {

    companion object {
        fun showSonnetDetailActivity(
            activity: Activity,
            sonnetModel: SonnetModel,
            searchKeyword: String = ""
        ) {
            val intent = Intent(activity, SonnetDetailActivity::class.java)
            intent.putExtra(Argument.prc_sonnet, sonnetModel)
            intent.putExtra(Argument.srl_int, sonnetModel.id)
            intent.putExtra(Argument.srl_str, searchKeyword)
            activity.startActivity(intent)
        }

        fun replaceFragment(
            manager: FragmentManager, @IdRes containerId: Int,
            fragment: BaseFragment<*, *>, tag: String = fragment::class.java.simpleName
        ) {
            manager.beginTransaction().replace(containerId, fragment, tag).commit()
        }
    }

    class Argument {
        companion object {
            const val prc_sonnet = "prc_sonnet"
            const val srl_int = "srl_int"
            const val srl_str = "srl_str"
        }
    }

}